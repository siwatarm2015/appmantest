package mobileapp.com.newtestcandidate.model

import com.google.gson.annotations.SerializedName
import org.json.JSONObject


data class JsonListModel(
        @SerializedName("Id")
        val Id: String,
        @SerializedName("firstName")
        val firstName: String,
        @SerializedName("lastName")
        val lastName: String,
        @SerializedName("data")
        val dataList: List<JsonData> = listOf()
)

data class JsonData (
        @SerializedName("docType")
        val docType: String,
        @SerializedName("description")
        val description: Description
)

data class Description(
        @SerializedName("th")
        val th: String,
        @SerializedName("en")
        val en: String
)
