package mobileapp.com.newtestcandidate.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_data_list.*
import mobileapp.com.newtestcandidate.R
import mobileapp.com.newtestcandidate.adapter.ListAdapter
import mobileapp.com.newtestcandidate.model.JsonListModel
import mobileapp.com.newtestcandidate.utils.JsonHelper
import mobileapp.com.newtestcandidate.utils.Utils

class DataListActivity : AppCompatActivity() {

    private lateinit var dataList: JsonListModel
    private lateinit var adapterList: ListAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data_list)
        init()
    }

//  TODO("DataList")
    private fun init(){
        adapterList = ListAdapter(this,{})
        with(recycler_view){
            adapter = adapterList
            layoutManager = LinearLayoutManager(this@DataListActivity,LinearLayoutManager.VERTICAL,false)
        }

        getData()
    }


    private fun getData() {
        val jsonString = Utils.loadJSONFromAsset("json.json", this)
        val listType = object : TypeToken<JsonListModel>() {}.type
//        dataList = JsonHelper.toClasss(JSONObject(jsonString), JsonListModel::class.java).dataList
        dataList = JsonHelper.toModel(jsonString,listType)
        adapterList.setData(dataList.dataList)

    }

}
