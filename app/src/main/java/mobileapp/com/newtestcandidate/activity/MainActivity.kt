package mobileapp.com.newtestcandidate.activity

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.activity_main.*
import mobileapp.com.newtestcandidate.R
import mobileapp.com.newtestcandidate.adapter.MenuAdapter

class MainActivity : AppCompatActivity() {

    private val menuList: List<String> by lazy { listOf("json", "alert", "image","camera") }


    private lateinit var adapterList: MenuAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {

        adapterList = MenuAdapter(this, onClick)

        with(recycler_view) {
            adapter = adapterList
            layoutManager = LinearLayoutManager(this@MainActivity)
        }

        getData()
    }


    private fun getData() {
        adapterList.setData(menuList)
    }

    private val onClick: (positon: Int) -> Unit = { position ->
        when (position) {
            0 -> {
                startPage(DataListActivity::class.java)
            }
            1 -> {
                startPage(AlertDialogActivity::class.java)
            }
            2 -> {
                startPage(ImageActivity::class.java)
            }
            3->{
//  TODO("Open Camera")
                askPermission()
            }
        }

    }

    private fun askPermission() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA
                ).withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                        val intent = Intent("android.media.action.IMAGE_CAPTURE")
                        startActivity(intent)
                    }
                    override fun onPermissionRationaleShouldBeShown(
                            permissions: List<PermissionRequest>,
                            token: PermissionToken
                    ) {
                        // request permission when call method again
                        token.continuePermissionRequest()

                        // ask permission once time
                        token.cancelPermissionRequest()
                    }
                }).check()

    }

    private fun startPage(act: Class<*>) {
        val intentAct = Intent(this, act)
        startActivity(intentAct)
    }


}
